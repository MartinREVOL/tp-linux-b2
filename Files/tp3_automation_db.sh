#./tp3_automation_db.sh
dnf install mariadb-server
systemctl start mariadb
systemctl enable mariadb
mysql_secure_installation
firewall-cmd --add-port=3306/tcp --permanent
firewall-cmd --reload
mysql -u root -p
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
FLUSH PRIVILEGES;
