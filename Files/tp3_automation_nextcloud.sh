#./tp3_automation_nextcloud.sh
dnf install httpd
systemctl start httpd
systemctl enable httpd
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --reload
dnf install mysql
systemctl start mysql
systemctl enable mysql
dnf config-manager --set-enabled crb
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
module list php
dnf module enable php:remi-8.1 -y
dnf install -y php81-php
dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
dnf install unzip
dnf install wget
mkdir /var/www/tp2_nextcloud/
wget "https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip"
unzip nextcloud-25.0.0rc3.zip
mv nextcloud/* /var/www/tp2_nextcloud/
mv nextcloud/. /var/www/tp2_nextcloud/
mv nextcloud/.. /var/www/tp2_nextcloud/
chown apache:apache /var/www//tp2_nextcloud/ -R
echo "<VirtualHost *:80>      
  DocumentRoot /var/www/tp2_nextcloud/
  ServerName  web.tp2.linux
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>" >> /etc/httpd/conf.d/fichier.conf
systemctl start httpd
systemctl enable httpd
