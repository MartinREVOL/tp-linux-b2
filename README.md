# B2 Linux 2022

Voici mes TPs et fichiers demandés...

### ➜ **[TP](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/TP)**

- [TP 1 : Mise en jambe](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP1.md)
- [TP 2 : Gestion de services](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP2.md)
- [TP 3 :  Amélioration de la solution NextCloud](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/TP/TP3)
    - [Module 1 : Reverse Proxy](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP3/1-reverse-proxy.md)
    - [Module 5 : Monitoring](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP3/5-monitoring.md)
    - [Module 6 : Automatiser le déploiement](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP3/6-automatisation.md)
    - [Module 7 : Fail2Ban](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP3/7-fail2ban.md)
- [TP 4 : Conteneurs](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP4.md)
- [TP 5 : Solution](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/TP/TP5.md)

### ➜ **[Files](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/Files)**

- [app](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/Files/app)
    - [Dockerfile](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/Files/app/Dockerfile)
    - [docker-compose.yml](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/Files/app/docker-compose.yml)
- [Dockerfile](https://gitlab.com/MartinREVOL/tp-linux-b2/-/tree/main/Files/Dockerfile)
- [httpd.conf](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/httpd.conf)
- [tp3_automation_db.sh](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/tp3_automation_db.sh)
- [tp3_automation_nextcloud.sh](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/tp3_automation_nextcloud.sh)
- [jail.local](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/jail.local)
- [health_alarm_notify.conf](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/health_alarm_notify.conf)