# TP4 : Conteneurs
# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/) :
    - Uninstall old versions :
    ```
    sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
    ```
    - Install using the repository :
    ```bash
    $ sudo yum install -y yum-utils

    $ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo 

    $ sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin

    $ sudo systemctl start docker

    $ sudo systemctl enable docker
    ```
    - test :
    ```bash
    [toto@docker1 ~]$ sudo docker run hello-world
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    2db29710123e: Pull complete 
    Digest: sha256:faa03e786c97f07ef34423fccceeec2398ec8a5759259f94d99078f264e9d7af
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/
    ```
- ajouter votre utilisateur au groupe `docker` :
```bash
[toto@docker1 ~]$ sudo usermod -aG docker toto
[toto@docker1 ~]$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```


## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

```bash
# Info sur l'install actuelle de Docker
[toto@docker1 ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
[...]

# Liste des conteneurs actifs
[toto@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

# Liste de tous les conteneurs
[toto@docker1 ~]$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS     NAMES
7ded1a1eeaf3   hello-world   "/hello"   4 minutes ago    Exited (0) 4 minutes ago              eloquent_gagarin
48d21edb1729   hello-world   "/hello"   11 minutes ago   Exited (0) 11 minutes ago             wizardly_tu

# Liste des images disponibles localement
[toto@docker1 ~]$ docker images
REPOSITORY    TAG       IMAGE ID       CREATED         SIZE
hello-world   latest    feb5d9fea6a5   14 months ago   13.3kB

# Lancer un conteneur debian
[toto@docker1 ~]$ docker run debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
a8ca11554fce: Pull complete 
Digest: sha256:3066ef83131c678999ce82e8473e8d017345a30f5573ad3e44f62e5c9c46442b
Status: Downloaded newer image for debian:latest

[toto@docker1 ~]$ docker run -d debian sleep 20
78b251a577400ba6f5fd5a5e603663b24126c42235321748dd96ac340e705b56

[toto@docker1 ~]$ docker run -it debian bash
root@3b983d7253ee:/# 

# Consulter les logs d'un conteneur
# ID = a7b6b603e9c8
[toto@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND        CREATED         STATUS         PORTS     NAMES
a7b6b603e9c8   debian    "sleep 1000"   4 seconds ago   Up 3 seconds             musing_varahamihira 

[toto@docker1 ~]$ docker logs a7b6b603e9c8

[toto@docker1 ~]$ docker logs -f a7b6b603e9c8
[...]

# Exécuter un processus dans un conteneur actif
[toto@docker1 ~]$ docker exec a7b6b603e9c8 ls
bin
boot
dev
etc
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var

[toto@docker1 ~]$ docker exec -it a7b6b603e9c8 bash
root@a7b6b603e9c8:/# 
root@a7b6b603e9c8:/# 
root@a7b6b603e9c8:/# 
```

## 3. Lancement de conteneurs
```bash
# L'option --name permet de définir un nom pour le conteneur
[toto@docker1 ~]$ docker run --name web nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
a603fa5e3b41: Pull complete 
c39e1cda007e: Pull complete 
90cfefba34d7: Pull complete 
a38226fb7aba: Pull complete 
62583498bae6: Pull complete 
[...]

# L'option -d permet de lancer un conteneur en tâche de fond
[toto@docker1 ~]$ docker run --name web -d --rm nginx
d25017a41a7fc8fab0e7e5de82ef5dc2451a47a43639c8a12702f5109d7ed259

# L'option -v permet de partager un dossier/un fichier entre l'hôte et le conteneur
$ docker run --name web -d -v /path/to/html:/usr/share/nginx/html nginx

# L'option -p permet de partager un port entre l'hôte et le conteneur
$ docker run --name web -d -v /path/to/html:/usr/share/nginx/html -p 8888:80 nginx
# Dans l'exemple ci-dessus, le port 8888 de l'hôte est partagé vers le port 80 du conteneur
```

🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - On créé un ficher de conf :
  ```
  [toto@docker1 ~]$ sudo cat ui.conf 
    [sudo] password for toto: 
    server {
    # on définit le port où NGINX écoute dans le conteneur
    listen 9999;
    
    # on définit le chemin vers la racine web
    # dans ce dossier doit se trouver un fichier index.html
    root /var/www/tp4; 
    }
  ```
  - On créé un ficher index.html :
  ```
  [toto@docker1 ~]$ sudo cat index.html 
    <!DOCTYPE html>
    <html>
    <body>

    <h1>My First Heading</h1>

    <p>My first paragraph.</p>

    </body>
    </html>
  ```

La commande --> 
```
[toto@docker1 ~]$ docker run --name test -d -v $(pwd)/ui.conf:/etc/nginx/conf.d/ui.conf -v $(pwd)/index.html:/var/www/tp4/index.html -p 8888:9999 -m 70m --cpus=1 nginx
70281e05a801be365c61a32e24f4a67a7318e0b798c91a15ebe18dbdf0bb5041
```

![](https://cdn.discordapp.com/attachments/713411498281795665/1045310291149336586/Capture_decran_2022-11-24_a_13.09.27.png)

# II. Images

🌞 **Construire votre propre image**

- On créé on dossier pour travailler :
  ```
  sudo mkdir Dockerplace
  ```
- On créé le fichier Dockerfile dans Dockerplace :
  ```
  sudo nano Dockerplace/Dockerfile
  ```
- On construit une image :
  ```
  docker build . -t zapata
  ```
- On créé un fichier de conf :
  ```
  [toto@docker1 ~]$ sudo cat Dockerplace/httpd.conf 
  [sudo] password for toto: 
  # on définit un port sur lequel écouter
  Listen 80

  # on charge certains modules Apache strictement nécessaires à son bon fonctionnement
  LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
  LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
  LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

  # on indique le nom du fichier HTML à charger par défaut
  DirectoryIndex index.html
  # on indique le chemin où se trouve notre site
  DocumentRoot "/var/www/html/"

  # quelques paramètres pour les logs
  ErrorLog "logs/error.log"
  LogLevel warn
  ```
- On modifie le Dockerfile...
- On lance le tout :
  ```
  docker run -d -p 8888:80 zapata
  ```

[📁 **Dockerfile**](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/Dockerfile)

# III. `docker-compose`

🌞 **Conteneurisez votre application**

- créer un `Dockerfile` maison qui porte l'application :
  ```bash
  [toto@docker1 ~]$ sudo cat app/Dockerfile 
  [sudo] password for toto: 
  FROM ubuntu

  RUN apt update -y

  RUN apt install -y golang

  RUN apt install -y git

  RUN git clone "https://gitlab.com/MartinREVOL/hangmanweb.git" /app

  WORKDIR /app/Web

  CMD [ "go", "run", "main.go" ]
  ```
- créer un `docker-compose.yml` qui permet de lancer votre application :
  ```bash
  [toto@docker1 ~]$ sudo cat app/docker-compose.yml 
  version: "3.8"

  services:
    go:
      image: test
      restart: always
      ports:
        - '8080:8080'
  ```
- vous préciserez dans le rendu les instructions pour lancer l'application :

  - On va dans le dossier **/app** et on écrit 
    ```
    $ docker build . -t hangman
    ```
    ```
    $ docker-compose up -d
    ```
  - On se connecte au port 8080 :
    
    **Malheureusement**, je ne pourrais pas te le montrer car visiblement y'a une erreur de dev de mon `hangman` : J'ai essayé pleins de trucs y compris changer la version de golang, mais rien n'y fait désolé...

![THE END](https://cdn.discordapp.com/attachments/713411498281795665/1047831317573812325/giphy.gif)
