# TP1 : (re)Familiaration avec un système GNU/Linux
# 0. Préparation de la machine
Donc voilà j'ai deux VMs...
**On va leur donner une IP static :**

Pour la première (10.101.1.11/24) :
```
[toto@localhost ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.11
NETMASK=255.255.255.0
```
et pour la deuxième (10.101.1.12/24) :
```
[toto@localhost ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.101.1.12
NETMASK=255.255.255.0
```
Maintenant **on va pouvoir ping d'une machine à l'autre**, par exemple `10.101.1.11` ping `10.101.1.12`:
```
[toto@localhost ~]$ ping 10.3.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.80 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=1.20 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=1.20 ms
```

Et aussi ping **internet** :

`10.101.1.11` ping `1.1.1.1` : 
```
[toto@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=28.4 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=31.0 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=35.2 ms
```

same avec `10.3.1.11` :
```
[toto@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=28.4 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=31.0 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=63 time=35.2 ms
```

**Connection en SSH** :
C'est simple on se connect en SSH avec la cmd --> ssh toto@<IP_LOCALE>

Par exemple :
```
ssh toto@10.101.1.11
```

**On ajoute un nom** :
Facile ! On écrit le nom qu'on veut dans le fichier ***/etc/hostname*** :

On reboot et tada ! 
```
[toto@node1 ~]$
```
```
[toto@node2 ~]$
```

Si on veut que le DNS `1.1.1.1`, On va dans ***/etc/resolv.conf*** et on efface tout sauf la ligne avec `1.1.1.1`.

Pour vérifier on fait un ***dig ynov.com*** : 
```
[toto@node1 ~]$ sudo dig ynov.com

; <<>> DiG 9.16.23-RH <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46236
;; flags: qr rd ra; QUERY: 1, ANSWER: 3, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	172.67.74.226

;; Query time: 32 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Nov 14 12:39:51 CET 2022
;; MSG SIZE  rcvd: 85
```

IP qui correspond au nom demandé :
```
ynov.com.		300	IN	A	104.26.10.233
ynov.com.		300	IN	A	104.26.11.233
ynov.com.		300	IN	A	172.67.74.226
```

IP du serveur qui a répondu :
```
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

On va ensuite rajouter ses deux lignes dans le fichier ***/etc/hosts*** des deux VMs :
```
10.101.1.11   node1.tp1.b2 localhost.localdomain localhost4 localhost4.localdomain4
````
```
10.101.1.12   node2.tp1.b2 localhost.localdomain localhost4 localhost4.localdomain4
```

Ça va nous permettre de pouvoir fzaire en sorte que les deux VMs puissent se ping avec leurs *hostname*.

Exemple --> `node1`(10.101.1.11) ping `node2`(10.101.1.12) :
```
[toto@node1 ~]$ ping node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.645 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.10 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=1.07 ms
```

# I. Utilisateurs

## 1. Création et configuration
### 🌞 Ajouter un utilisateur à la machine
Pour ça on fait (dans ***/home/***):
```
sudo useradd volverin -m -s /bin/bash
```
Un petit ls pour verifier :
```
[toto@node1 home]$ ls
toto  volverin
```

### 🌞 Créer un nouveau groupe admins
Pour créer un nouveau groupe : 
```
sudo groupadd Xmen
```
Et on rajoute les droits admin au groupe `Xmen` (dans /etc/sudoers) grace à la commande ***sudo visudo /etc/sudoers***:
```
%Xmen  ALL=(ALL)       ALL
```

### 🌞 Ajouter votre utilisateur à ce groupe admins
```
[toto@node1 ~]$ sudo usermod -aG Xmen volverin
[toto@node1 ~]$ groups volverin
volverin : volverin Xmen
```

## 2. SSH
- Génération d'une clé :
`ssh-keygen -t rsa -b 4096` ;
- Dépos de la clé :
`ssh-copy-id <USER>@<ADRESSE_IP>` ;
- Connection :
`ssh <USER>@<ADRESSE_IP>` :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % ssh toto@10.101.1.11 
Last login: Mon Nov 14 13:36:59 2022 from 10.101.1.1
[toto@node1 ~]$
```

# II. Partitionnement
## 1. Préparation de la VM

