➜ **Ecrire le script `bash`** :

- Tu peux télécharger mon script [ici](./Files/tp3_automation_nextcloud.sh).

- Tu peux le transférer depuis ton PC, vers ta VM avec `scp /path/to/file username@a:/path/to/destination`

- Et tu peux le lancer avec `sudo bash ./tp3_automation_nextcloud.sh`

✨ **Bonus** :

- Tu peux télécharger mon script [ici](./Files/tp3_automation_db.sh).

- Tu peux le transférer depuis ton PC, vers ta VM avec `scp /path/to/file username@a:/path/to/destination`

- Et tu peux le lancer avec `sudo bash ./tp3_automation_db.sh`

![](https://cdn.discordapp.com/attachments/713411498281795665/1045083599831060490/voila.gif)
