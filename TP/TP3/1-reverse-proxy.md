# Module 1 : Reverse Proxy
# I. Setup

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx` :
```
sudo dnf install nginx
```
- démarrer le service `nginx` :
```
[toto@proxy ~]$ sudo systemctl start nginx
[toto@proxy ~]$
[toto@proxy ~]$ sudo systemctl status nginx
[...]
Active: active (running)...
[...]
```
- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute :
```
[toto@proxy ~]$ sudo ss -alnpt | grep nginx
[sudo] password for toto: 
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1372,fd=6),("nginx",pid=1371,fd=6))
LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=1372,fd=7),("nginx",pid=1371,fd=7))
```
soit en écoute sur le port 80.
- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX :
    - on ouvre
    ```
    sudo firewall-cmd --add-port=80/tcp --permanent
    ```
    - on reload
    ```
    sudo firewall-cmd --reload
    ```
- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX :
```
[toto@proxy ~]$ ps -ef | grep nginx
nginx       1372    1371  0 09:47 ?        00:00:00 nginx: worker process
```
- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine : 
```
[toto@proxy ~]$ curl 10.102.1.13:80
<!doctype html>
<html>
  <head>
...
```

➜ **Configurer NGINX**

- créer un fichier de configuration NGINX :
```
[toto@proxy ~]$ sudo cat /etc/nginx/conf.d/ui.conf 
[sudo] password for toto: 
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://10.102.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
- NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy :
```bash
[toto@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php 
[sudo] password for toto: 
<?php
$CONFIG = array (
  'instanceid' => 'oc1k5g2o7fxu',
  'passwordsalt' => 'kANk7gQS5DE0AwUY8yaJWp61QpO+d9',
  'secret' => 'pYNlB744fjUwbMYmx0FLUGY9FxoUyU6lqp+tNad1Kgzs/hIA',
  'trusted_domains' => 'web.tp2.linux',
  'overwritehost' => 'web.tp2.linux',
  'trusted_proxies' => '10.102.1.13',
  'overwriteprotocol' => 'http',
  array (
    0 => 'web.tp2.linux',
  ),
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'pewpewpew',
  'installed' => true,
);
```
➜ **Modifier votre fichier hosts de VOTRE PC**

```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo cat /etc/hosts
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost
10.102.1.13	web.tp2.linux
10.102.1.12	db.tp2.linux
```
On tape --> http://web.tp2.linux :
![](https://cdn.discordapp.com/attachments/713411498281795665/1043105748898566165/Capture_decran_2022-11-18_a_11.09.27.png)

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
  - une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
  - l'autre est la clé publique : elle sera stockée dans un fichier appelé *certificat*
    - le *certificat* est donné à chaque client qui se connecte au site

```bash
[toto@proxy ~]$ ls -l
total 8
-rw-r--r--. 1 val val 1444 Nov 21 10:57 certficat.crt
-rw-------. 1 val val 1704 Nov 21 10:56 server.key
```

- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP

```bash
[toto@proxy ~]$ sudo cat /etc/nginx/conf.d/clair.conf
server {
    server_name web.tp2.linux;

    listen 443 ssl;

    ssl                       on;
    ssl_certificate           /home/val/server.crt;
    ssl_certificate_key       /home/val/server.key;
    ssl_protocols             TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;

    location / {
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_pass https://10.102.1.11:80;
    }

    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

- *on ouvre le port `443`*

```bash
[toto@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[toto@proxy ~]$ sudo firewall-cmd --reload
success
```
**TADAAAAAA**

![](https://cdn.discordapp.com/attachments/1043129905178746894/1044210071502073906/Capture_decran_2022-11-21_a_12.17.27.png)

VOILÀ

![](https://acegif.com/wp-content/uploads/gifs/the-end-3.gif)
