# Module 5 : Monitoring
➜ **liste des cmd trouvées [ici](https://wiki.crowncloud.net/?How_to_Install_Netdata_on_Rocky_Linux_9#Installing+EPEL+Repo)** (qu'on va taper dans notre machine) :
-  dnf update
-  sudo dnf update
-  sudo dnf install epel-release -y
-  sudo wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
-  sudo systemctl start netdata
-  sudo systemctl enable netdata
-  sudo systemctl status netdata
-  sudo firewall-cmd --permanent --add-port=19999/tcp ; sudo firewall-cmd --reload

On va check sur le web :

Y'a que la capture de db mais tkt j'ai fait pour tout le web aussi :))
![](https://cdn.discordapp.com/attachments/713411498281795665/1043129582972325918/Capture_decran_2022-11-18_a_12.44.02.png)


➜ **On vas configurer Netdata pour qu'il vous envoie des alertes** :

-on doit créer un Webhook discord :

![](https://cdn.discordapp.com/attachments/905799668938723329/1043132537263894620/image.png)

- on modifie `/etc/netdata/edit-config health_alarm_notify.conf` : 
```
[toto@web ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
SEND_DISCORD="YES"

DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1043130101195362334/fBxsqFfgncKphrF8f5HfUqNqqSGVKbHE1cQUF_tIfjzwSoAbFo53kcOOP9QENNx8RlfK"

DEFAULT_RECIPIENT_DISCORD="alarms"
DEFAULT_RECIPIENT_SLACK="#"
```
- on remplace le lien du Webhook par le notre et techniquement ça devrait être bon

- et tchakkk
![](https://cdn.discordapp.com/attachments/1043129905178746894/1044218341352558592/image.png)

### FIN
![](https://acegif.com/wp-content/uploads/gifs/the-end-11.gif)
