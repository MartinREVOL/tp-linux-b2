# TP2 : Gestion de service
# I. Un premier serveur web
## 1. Installation
**🌞 Installer le serveur Apache**

On install `httpd` :
```
sudo dnf install httpd
```

**🌞 Démarrer le service Apache**

On lance le serv avec :
```
sudo systemctl start httpd
```
et si on veux qu'il se lance de base :
```
sudo systemctl enable httpd
```
On va ensuite check sur quel port se situe le serv :
```
[toto@web ~]$ sudo ss -alnpt | grep httpd
LISTEN 0      511                *:80              *:*    users:(("httpd",pid=1232,fd=4),("httpd",pid=1231,fd=4),("httpd",pid=1230,fd=4),("httpd",pid=1228,fd=4))
```
On ouvre le port firewall 80 :
```
[toto@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[toto@web ~]$ sudo firewall-cmd --reload
success
```
**🌞 TEST**

On vérifie si le serv est bien actif au démarrage :
```
[toto@web ~]$ sudo systemctl status httpd
     Active: active (running) since Tue 2022-11-15 10:33:32 CET; 20min ago
```
On vérifie si le firewall est bien actif :
```
[toto@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```
On vérifie que le serv web est joignable localement :
```
[toto@web ~]$ curl 10.102.1.11:80
<!doctype html>
<html>
  <head>
...
```
On vérifie que le serv web est joignable dans un navigateur -->
On tape 10.102.1.11:80 dans notre navigateur :
![](https://cdn.discordapp.com/attachments/713411498281795665/1042016129113129001/Capture_decran_2022-11-15_a_10.58.51.png)
## 2. Avancer vers la maîtrise du service
**🌞 Le service Apache...**

On ouvre le fichier pour avoir la definition :
```
[toto@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```
**🌞 Déterminer sous quel utilisateur tourne le processus Apache**

***Quel user est utilisé ?*** --> `apache`
```
[toto@web ~]$ sudo cat /etc/httpd/conf/httpd.conf 
[sudo] password for toto: 
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost
```
On confirme avec :
```
[toto@web ~]$ ps -ef | grep apache
apache      1229    1228  0 10:33 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1230    1228  0 10:33 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1231    1228  0 10:33 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      1232    1228  0 10:33 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
toto        1589    1194  0 11:41 pts/0    00:00:00 grep --color=auto apache
```
apache peut accéder à toute la page d'accueil d'Apache :
```
[toto@web testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Nov 15 10:16 .
drwxr-xr-x. 80 root root 4096 Nov 15 10:16 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```
**🌞 Changer l'utilisateur utilisé par Apache**

On créé un nouvel user --> On ajoute cette ligne :
```
[toto@web httpd]$ sudo nano /etc/passwd
xavier:x:2000:2000:Apache:/usr/share/httpd:/sbin/nologin
```
--> On modifie le User de la conf de Apache :
```
[toto@web httpd]$ sudo cat /etc/httpd/conf/httpd.conf
ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User xavier
Group apache


ServerAdmin root@localhost
```
On vérifie après reboot :
```
[toto@web ~]$ ps -ef | grep xavier
xavier       707     681  0 12:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
xavier       710     681  0 12:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
xavier       711     681  0 12:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
xavier       712     681  0 12:20 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        1430    1408  0 12:21 pts/0    00:00:00 grep --color=auto xavier
```
**🌞 Faites en sorte que Apache tourne sur un autre port**

On modifie la conf :
```
[toto@web ~]$ sudo cat /etc/httpd/conf/httpd.conf
ServerRoot "/etc/httpd"

Listen 211

Include conf.modules.d/*.conf

User xavier
Group apache


ServerAdmin root@localhost
```
On ouvre le port 211 dans le firewall, et on ferme le 80 :

- on ouvre
```
sudo firewall-cmd --add-port=211/tcp --permanent
```
- on ferme
```
sudo firewall-cmd --remove-port=80/tcp --permanent
```
- on reload
```
sudo firewall-cmd --reload
```
On check :
```
[toto@web ~]$ sudo ss -alnpt | grep httpd
[sudo] password for toto: 
LISTEN 0      511                *:211             *:*    users:(("httpd",pid=713,fd=4),("httpd",pid=712,fd=4),("httpd",pid=711,fd=4),("httpd",pid=685,fd=4))
```
On vérifie que le serv web est joignable localement :
```
[toto@web ~]$ curl 10.102.1.11:211
<!doctype html>
<html>
  <head>
...
```
On vérifie que le serv web est joignable dans un navigateur -->
On tape 10.102.1.11:211 dans notre navigateur :
![](https://cdn.discordapp.com/attachments/713411498281795665/1042016129113129001/Capture_decran_2022-11-15_a_10.58.51.png)

### **[📁 Fichier /etc/httpd/conf/httpd.conf](https://gitlab.com/MartinREVOL/tp-linux-b2/-/blob/main/Files/httpd.conf)**

# II. Une stack web plus avancée
## 1. Intro blabla
blabla
## 2. Setup
### A. Base de données
**🌞 Install de MariaDB sur db.tp2.linux**

On suit la doc de [Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/) :
- We need to install mariadb-server:

```
sudo dnf install mariadb-server
```
- To strengthen the security of mariadb-server we need to run a script, but before we do, we need to enable and start mariadb:

```
sudo systemctl enable mariadb
```
- And then:

```
sudo systemctl start mariadb
```
- Next, run this command:

```
sudo mysql_secure_installation
```
- Tape your password:
- Tape "y" and "Enter": (Remove anonymous users? [Y/n])
- Tape "Enter": (Disallow root login remotely? [Y/n])
- Tape "Enter": (Remove test database and access to it? [Y/n])
- Tape "Enter": (Reload privilege tables now? [Y/n])

**On repère quel port utilise MariaDB** :
```
[toto@db ~]$ sudo ss -alnpt | grep maria
[sudo] password for toto: 
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3506,fd=19))
```
soit port `3306`.

On l'ouvre dans le firewall :
- on ouvre
```
sudo firewall-cmd --add-port=3306/tcp --permanent
```
- on reload
```
sudo firewall-cmd --reload
```

**🌞 Préparation de la base pour NextCloud**

On se connecte à la base de données à l'aide de la commande `sudo mysql -u root -p`, et on y exécute les commandes SQL suivantes :
```bash
# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.linux
# "pewpewpew" c'est le mot de passe hehe
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;

# C'est assez générique comme opération, on crée une base, on crée un user, on donne les droits au user sur la base
```
**🌞 Exploration de la base de données**

- On install mysql sur la VM web.tp2.linux : 
```
sudo dnf install mysql
```

- On se connecte au NextCloud depuis la machine web.tp2.linux vers l'IP de db.tp2.linux et on explore :
```
[toto@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
mysql> 
mysql> 
mysql> 
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.01 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.01 sec)

mysql> 
```
**🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**
```
MariaDB [(none)]> SELECT User, Host FROM mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
4 rows in set (0.009 sec)
```
### B. Serveur Web et NextCloud
**🌞 Install de PHP**

```bash
# On ajoute le dépôt CRB
$ sudo dnf config-manager --set-enabled crb
# On ajoute le dépôt REMI
$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
$ dnf module list php

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
$ sudo dnf module enable php:remi-8.1 -y

# Eeeet enfin, on installe la bonne version de PHP : 8.1
$ sudo dnf install -y php81-php
```
**🌞 Install de tous les modules PHP nécessaires pour NextCloud**
```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```
**🌞 Récupérer NextCloud**

- On install "unzip" et "wget".
- On créé le dossier /var/www/tp2_nextcloud/ :
```
sudo mkdir /var/www/tp2_nextcloud/
```
- On récupère le fichier :
```
sudo wget "https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip"
```
- On l'extrait dans /var/www/tp2_nextcloud/ :
```
unzip nextcloud-25.0.0rc3.zip
```
```
sudo mv nextcloud/* /var/www/tp2_nextcloud/
```
- On cherche le fichier index.html :
```
[toto@web ~]$ ls -l /var/www/tp2_nextcloud/ | grep index
-rw-r--r--.  1 toto toto   156 Oct  6 14:42 index.html
```
- On fait en sorte que le fichier appartienne à l'user apache :
```
[toto@web ~]$ sudo chown apache:apache /var/www//tp2_nextcloud/ -R
[toto@web ~]$ ls -al /var/www/tp2_nextcloud/
total 140
drwxr-xr-x. 14 apache apache  4096 Nov 17 11:53 .
drwxr-xr-x.  5 root   root      54 Nov 17 11:14 ..
-rw-r--r--.  1 apache apache  3253 Oct  6 14:42 .htaccess
-rw-r--r--.  1 apache apache   101 Oct  6 14:42 .user.ini
drwxr-xr-x. 47 apache apache  4096 Oct  6 14:47 3rdparty
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
drwxr-xr-x.  2 apache apache    67 Oct  6 14:47 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
drwxr-xr-x. 23 apache apache  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache apache  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:42 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
```
**🌞 Adapter la configuration d'Apache**

```
[toto@web ~]$ sudo cat /etc/httpd/conf.d/ui.conf
<VirtualHost *:80>
  DocumentRoot /var/www/tp2_nextcloud/ 
  ServerName  web.tp2.linux
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
**🌞 Redémarrer le service Apache**

```
sudo systemctl status httpd
```
### C. Finaliser l'installation de NextCloud
**➜ Sur votre PC**
- Pouvoir joindre l'IP de la VM en utilisant le nom web.tp2.linux :
```
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % sudo cat /etc/hosts
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1	localhost
255.255.255.255	broadcasthost
::1             localhost
10.102.1.11	web.tp2.linux
revol-buisson@MacBook-Pro-de-REVOL-BUISSON ~ % ssh toto@web.tp2.linux
Last login: Thu Nov 17 12:08:57 2022 from 10.102.1.0
[toto@web ~]$
```
- Pouvoir joindre l'IP de la VM en utilisant son navigateur :

![](https://cdn.discordapp.com/attachments/713411498281795665/1043078103779508274/Capture_decran_2022-11-18_a_09.19.37.png)

**🌞 Exploration de la base de données**

```
MariaDB [nextcloud]> SHOW TABLES;
95 rows in set (0.001 sec)
```
soit 95 tables ont été crées par NextCloud lors de la finalisation de l'installation.

![](https://cdn.discordapp.com/attachments/713411498281795665/1043081180624453643/the-end-18.gif)
