# TP5 : Mise en place d'une solution
## Sommaire
Martin et Valentin

- [TP5 : Mise en place d'une solution](#tp5--mise-en-place-d'une-solution)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
  - [II. Partitionnement](#ii-partitionnement)
  - [III. Docker](#iii-docker)
    - [1. Installation de Docker](#1-installation-de-docker)
  - [IV. Mise en place](#iv-mise-en-place)
    - [1. Installation de la solution](#1-installation-de-la-solution)
    - [2. Fail2Ban](#2-fail2ban)
    - [3. Monitoring](#3-monitoring)


## 0. Préparation de la machine

**Setup d'une machine Rocky Linux configurée de façon basique.**

- **définir une ip static**

  ```bash
  [toto@chess ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
  DEVICE=enp0s8

  BOOTPROTO=static
  ONBOOT=yes

  IPADDR=10.105.1.11
  NETMASK=255.255.255.0
  ```

  - **on redémarre l'interface** 

    ```bash
    [toto@chess ~]$ sudo systemctl restart NetworkManager
    ```

- **un accès internet (via la carte NAT)**

  ```bash
  [toto@chess ~]$ ping google.com
  PING google.com (142.250.74.238) 56(84) bytes of data.
  64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=248 time=22.4 ms
  ```

- **nommer la machine doit avoir un nom**

  ```bash
  [toto@chess ~]$ sudo cat /etc/hostname
  [sudo] password for val:
  chatroom.tp5.linux
  ```

- **faire l'échange de clé ssh**

  - **créer le directory `.ssh` **

    ```bash
    [toto@chess ~]$ sudo mkdir /home/val/ssh
    ```

  - **ajouter notre clé**

    ```bash
    [toto@chess ~]$ sudo vim /home/val/.ssh/authorized_keys
    ```


## I. Utilisateurs

### 1. Création et configuration

**Ajouter un utilisateur à la machine**, qui sera dédié à son administration

- **on créer un utilisateur qui nous permettra d'administrer la solution**

  ```bash
  [toto@chess ~]$ sudo useradd valentin -m -s /bin/sh
  ```

  - **on vérifie avec un ls**

    ```bash
    [toto@chess /]$  ls home/
    val  valentin
    ```

- **on créer un groupe `admins` **

  ```bash
  [toto@chess /]$  sudo groupadd admins
  ```

  - **on donne au groupe `admins` les droits root**

    ```bash
    [val@chess /]$  sudo cat /etc/sudoers | grep -i admins
    # User_Alias ADMINS = jsmith, mikem
    %admins ALL=(ALL)       ALL
    ```

  - **on ajoute l'utilisateur à notre groupe `admins`**

    ```bash
    [toto@chess /]$ sudo usermod -aG admins valentin
    ```

  - **on vérifie s'il appartient bien à notre groupe `admins`**

    ```bash
    [toto@chess /]$ groups valentin
    valentin : valentin admins
    ```

## II. Partitionnement

- **on va créer nos deux disques depuis oracle VM depuis la configuration de notre vm (deux disques de 3Go)**

- **on regarde si ils ont bien été ajouté à notre vm**

  ```bash
  [toto@chatroom ~]$ lsblk | grep -E 'sdb|sdc'
  sdb           8:16   0    3G  0 disk
  sdc           8:32   0    3G  0 disk
  ```

- **on agrège nos deux disques en un *volume group* `2ndDisk`**

  ```bash
  [toto@chatroom ~]$ sudo vgcreate 2ndDisk /dev/sdb
  [sudo] password for val:
  Physical volume "/dev/sdb" successfully created.
  Volume group "2ndDisk" successfully created
  [toto@chatroom ~]$ sudo vgextend 2ndDisk /dev/sdc  
  Physical volume "/dev/sdc" successfully created.
  Volume group "2ndDisk" successfully extended
  ```

- **on créer 3 *logical volume* de 1Go chacun**

  ```bash
  [toto@chatroom ~]$ sudo lvcreate -L 1G 2ndDisk -n number1
  Logical volume "number1" created.
  [toto@chatroom ~]$ sudo lvcreate -L 1G 2ndDisk -n number2
  Logical volume "number2" created.
  [toto@chatroom ~]$ sudo lvcreate -L 1G 2ndDisk -n number3
  Logical volume "number3" created.
  ```

- **et on formate  les partitions**

  ```bash
  [toto@chatroom ~]$ sudo mkfs -t ext4 /dev/2ndDisk/number1
  mke2fs 1.46.5 (30-Dec-2021)
  Creating filesystem with 262144 4k blocks and 65536 inodes
  [...]
  Writing superblocks and filesystem accounting information: done

  [toto@chatroom ~]$ sudo mkfs -t ext4 /dev/2ndDisk/number2
  mke2fs 1.46.5 (30-Dec-2021)
  Creating filesystem with 262144 4k blocks and 65536 inodes
  [...]
  Writing superblocks and filesystem accounting information: done

  [toto@chatroom ~]$ sudo mkfs -t ext4 /dev/2ndDisk/number3
  mke2fs 1.46.5 (30-Dec-2021)
  Creating filesystem with 262144 4k blocks and 65536 inodes
  [...]
  Writing superblocks and filesystem accounting information: done
  ```

- **on va monter ces partitions**
  - **pour ça on va monter 3 points de montage**

    ```bash
    [toto@chatroom ~]$ mkdir mnt/part1
    [toto@chatroom ~]$ mkdir mnt/part2
    [toto@chatroom ~]$ mkdir mnt/part3
    ```

  - **on peut maintenant monter les partitions**

    ```bash
    [toto@chatroom ~]$ sudo mount /dev/2ndDisk/number1 mnt/part1
    [toto@chatroom ~]$ sudo mount /dev/2ndDisk/number2 mnt/part2
    [toto@chatroom ~]$ sudo mount /dev/2ndDisk/number3 mnt/part3
    ```

- **on modifie maintenant `/etc/fstab` afin qu'elles soient montées automatiquement au démarrage**

  ```bash
  [toto@chatroom ~]$ sudo cat /etc/fstab
  [...]
  /dev/2ndDisk/number1 /mnt/part1 ext4 defaults 0 0
  /dev/2ndDisk/number2 /mnt/part2 ext4 defaults 0 0
  /dev/2ndDisk/number3 /mnt/part3 ext4 defaults 0 0
  ```


## III. Docker
  ### 1. Installation de Docker

- **on installe yum**

  ```bash
  [toto@localhost ~]$ sudo yum install -y yum-utils
  ```

- **on ajoute le repo**

  ```bash
  [toto@localhost ~]$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  ```

- **on installe docker et tout ce qui lui faut**

  ```bash
  [toto@localhost ~]$ sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
  ```

- **et on demarre docker + demarrage automatique**

```bash
[toto@localhost ~]$ sudo systemctl start docker
[toto@localhost ~]$ sudo systemctl enable docker
```

## IV. Mise en place

### 1. Installation de la solution

**Nous avons choisi d'utiliser un mastermind où le couleurs sont remplacées par des mots**

- **Etape ZERO**
    - **Necessite d'installer *`git`***

- **Etape UNE**
    - **On clone le repo**

        ```bash
        [toto@localhost ~]$ git clone https://github.com/clupasq/word-mastermind.git
        ```
    - **On se place dans le dossier *`word-mastermind`***

        ```bash
        [toto@localhost ~]$ cd word-mastermind
        ```
        
- **Etape DEUX**
    - **Toujours dans le dossier cité au dessus on prépare l'image de docker**

        ```bash
        [toto@localhost word-mastermind]$ docker build -t word-mastermind .
        ```
    
    - **Puis on lance l'image**

        ```bash
        [toto@localhost word-mastermind]$ docker run --rm -p "3333:80" word-mastermind
        ```
        
- **Etape TROIS**
    - **On peut maintenant acceder à la solution en tapant *`http://(ip de la vm):3333`***
    


### 2. Fail2Ban

- **Etape UNE**
    - **On installe *`epel-release`***

        ```bash
        [toto@localhost ~]$ sudo dnf install epel-release
        ```
    - **On installe installe *`fail2ban`***

        ```bash
        [toto@localhost ~]$ sudo dnf install fail2ban fail2ban-firewalld
        ```
    
    - **On le start, on met le demarrage auto et on verifie son status**

        ```bash
        [toto@localhost ~]$ sudo systemctl start fail2ban
        [toto@localhost ~]$ sudo systemctl enable fail2ban
        Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
        [toto@localhost ~]$ sudo systemctl status fail2ban
        ● fail2ban.service - Fail2Ban Service
             Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
             Active: active (running) since Sat 2022-12-10 10:45:54 CET; 11s ago
               Docs: man:fail2ban(1)
          [...]
        ```

- **Etape DEUX**
    - **on copie le fichier de conf afin de pouvoir le modifier**

        ```bash
        [toto@localhost ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
        ```
        
        [jail.local](./tp5/fichiers/jail.local)
    
    - **ça nous permet de modifier les durées de ban, le nombre de tentatives...**

- **Etape TROIS**
    - **de base fail2ban fonctionne avec *`iptable`*, or, nous souhaitons utiliser *`firewalld`***

        ```bash
        [toto@localhost ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local
        ```
    
    - **on restart**

        ```bash
        [toto@localhost ~]$ sudo systemctl restart fail2ban
        ```
        
- **Etape QUATRE**
    - **on créer une prison**

        ```bash
        [toto@localhost ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local
        [toto@localhost ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
        [sshd]
        enabled = true

        # Override the default global configuration
        # for specific jail sshd
        bantime = 10m
        maxretry = 3
        ```
    - **on rerestart**

        ```bash
        [toto@localhost ~]$ sudo systemctl restart fail2ban
        ```
        
    - **on a notre prison**

        ```bash
        [toto@localhost ~]$ sudo fail2ban-client status
        Status
        |- Number of jail:      1
        `- Jail list:   sshd
        ```
        
- **Etape CINQ**
    - **on essaye depuis une autre vm de se connecter**

        ```bash
        [toto@proxy ~]$ ssh val@10.105.1.11
        The authenticity of host '10.105.1.11 (10.105.1.11)' can't be established.
        ED25519 key fingerprint is SHA256:nmKLJXCQWhry97nJGwX/9osqdQZE8sRmsUrKaW4DbeU.
        This key is not known by any other names
        Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
        Failed to add the host to the list of known hosts (/home/val/.ssh/known_hosts).
        val@10.105.1.11's password:
        Permission denied, please try again.
        val@10.105.1.11's password:
        Permission denied, please try again.
        val@10.105.1.11's password:
        val@10.105.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
        ```
        
    - **depuis notre vm on peut verifier la prison**

        ```bash
        [toto@localhost ~]$ sudo fail2ban-client status
        Status
        |- Number of jail:      1
        `- Jail list:   sshd
        [val@localhost ~]$ sudo fail2ban-client status sshd
        Status for the jail: sshd
        |- Filter
        |  |- Currently failed: 0
        |  |- Total failed:     3
        |  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
        `- Actions
           |- Currently banned: 1
           |- Total banned:     1
           `- Banned IP list:   10.105.1.1
        ```
    - **on peut check la rule du firewalld**

        ```bash
        [toto@localhost ~]$ sudo firewall-cmd --list-rich-rules
        rule family="ipv4" source address="10.105.1.1" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
        ```
        
    - **et voilà**


### 3. Monitoring

- **Etape UNE**
    - **on commence par installer netdata**

        ```bash
        [toto@localhost ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
        ```
        
    - **on start le service et on active le demarrage auto**

        ```bash
        [toto@localhost ~]$ sudo systemctl start netdata
        [toto@localhost ~]$ sudo systemctl enable netdata
        ```
    
- **Etape DEUX**
    - **on ouvre le port *`19999`***

        ```bash
        [toto@localhost ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
        success
        ```
        
    - **on relance le firewall pour qu'il prenne le changement en compte**

        ```bash
        [toto@localhost ~]$ sudo  firewall-cmd --reload
        success
        ```
        
    - **on peut check si ça a fonctionné en allant sur *`xx.xxx.x.xx:19999`* sur nore navigateur**

- **Etape TROIS**
    - **on voudrait recevoir des notifications quand la vm a du mal, on va donc faire ne sorte de recevoir des alertes sur discord**

    - **on va donc créer un webhook sur notre serveur discord**
    - **une fois créé on recupere son URL, ça va servir plus tard**
    - **on modifie le fichier de conf**

        ```bash
        [toto@localhost ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
        SEND_DISCORD="YES"

        DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1051090143496384572/4eHACnDweE1tGFDxh5yYaVP5LVCTWc1L4x2ZfTXKpiCpbM8GRblCz1h1xlLebg2aLuSQ"

        DEFAULT_RECIPIENT_DISCORD="alarms"
        DEFAULT_RECIPIENT_SLACK="#"
        ```
        
    - **on peut maintenant être au courant de l'état de la vm sans être à coté**

[health_alarm_notify.conf](./tp5/fichiers/health_alarm_notify.conf)
